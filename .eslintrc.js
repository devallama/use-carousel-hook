module.exports = {
    plugins: ['react', '@typescript-eslint', 'prettier'],
    extends: ['plugin:@typescript-eslint/recommended', 'prettier'],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: './tsconfig.json',
        tsconfigRootDir: __dirname
    },
    settings: {
        react: {
            version: '^16.0.0'
        }
    },
    rules: {
        /* react plugin rules */
        'react/jsx-wrap-multilines': [
            2,
            {
                declaration: 'parens-new-line',
                assignment: 'parens-new-line',
                return: 'parens-new-line',
                arrow: 'parens-new-line',
                condition: 'parens-new-line',
                logical: 'parens-new-line',
                prop: 'ignore'
            }
        ],
        'react/display-name': 0,
        'react/require-default-props': 0,
        'react/no-this-in-sfc': 2,
        'react/no-unsafe': 1,
        'react/no-unused-prop-types': 1,
        'react/no-unused-state': 1,
        'react/no-will-update-set-state': 2,
        'react/self-closing-comp': 2,
        'react/sort-comp': [
            1,
            {
                order: [
                    'static-variables',
                    'instance-variables',
                    'constructor',
                    'static-methods',
                    'lifecycle',
                    'everything-else',
                    'render'
                ]
            }
        ],
        'react/sort-prop-types': 1,
        'react/style-prop-object': 2,
        'react/jsx-curly-spacing': [2, { when: 'never', children: true }],
        'react/prop-types': [
            2,
            { ignore: ['children', 'history', 'theme', 'location', 'className'] }
        ],
        'react/jsx-tag-spacing': [
            2,
            {
                beforeSelfClosing: 'always',
                beforeClosing: 'never'
            }
        ],
        'react/jsx-closing-bracket-location': 2,
        'react/jsx-no-bind': [1, { allowArrowFunctions: true }],
        'react/jsx-pascal-case': 2,
        /* typescript plugin rules */
        'no-extra-parens': 0,
        '@typescript-eslint/no-extra-parens': 0,
        'no-duplicate-imports': 0,
        '@typescript-eslint/no-duplicate-imports': 2,
        '@typescript-eslint/typedef': 1,
        '@typescript-eslint/type-annotation-spacing': 2,
        '@typescript-eslint/no-unnecessary-type-arguments': 2,
        '@typescript-eslint/no-unnecessary-condition': 2,
        '@typescript-eslint/no-type-alias': 0,
        'no-magic-numbers': 0,
        '@typescript-eslint/no-magic-numbers': [1, { ignore: [-1, 0, 1, 2] }],
        'no-dupe-class-members': 0,
        '@typescript-eslint/no-dupe-class-members': 1,
        'no-array-constructor': 0,
        '@typescript-eslint/no-array-constructor': 2,
        'brace-style': 0,
        '@typescript-eslint/brace-style': 2,
        'no-invalid-this': 0,
        '@typescript-eslint/no-invalid-this': 2,
        /* prettier rules */
        'prettier/prettier': 2
    },
    overrides: [
        {
            files: ['**/*.test.tsx'],
            rules: {
                '@typescript-eslint/no-magic-numbers': 0
            }
        }
    ]
};
