module.exports = {
    rootDir: './',
    preset: 'ts-jest',
    testMatch: ['**/src/**/*.test.ts?(x)'],
    transform: {
        '^.+\\.(ts|tsx)$': '<rootDir>/node_modules/ts-jest'
    },
    testEnvironment: 'jsdom',
    collectCoverageFrom: ['src/**/*.ts?(x)', '!**/node_modules/**', '!src/index.ts'],
    coverageReporters: ['html', 'text', 'text-summary', 'cobertura']
};
