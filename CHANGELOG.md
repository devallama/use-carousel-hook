# Changelog

## [0.2.1]

- Fix test coverage report in .gitlab-ci.yml

## [0.2.0]

- Update README to give a correct example and link to demo on CodeSandbox

## [0.1.0]

- Can now pass the ref type to useCarousel using type casting
- 100% test coverage
- Now handles vertical scrolling
- (Potentially breaking) Commonjs is now the default module export. You can use the esnext build by importing `use-carousel-hook/esnext`
- Implement test coverage reporting on GitLab

## [0.0.16]

- Fix setMaxIndex bug

## [0.0.15]

- Exclude tests from build in cjs

## [0.0.14]

- Return children length if scrollWidth or scrollHeight of carousel is 0. This makes testing easier with @testing-library/react

## [0.0.13]

- Fix bug with scroll handler that updated `current` when programmatically scrolling
- Removed tests from dist
- Run tests on CI/CD

## [0.0.12]

- Remove files not needed from npm package to reduce size

## [0.0.11]

- Implemented automated publishing via GitLab CI/CD

## [0.0.10]

- Fix bug in carousel which was using an old version of current when handling scrolling

## [0.0.9]

- Update current and inView values on scroll

## [0.0.8]

- Update peerDep `@use-effect/did-update` to version 0.0.3

## [0.0.7]

- Build commonjs version accessible on use-carousel-hook/cjs

## [0.0.6]

- Fix bug with inView values not updating

## [0.0.5]

- Fix issue width lastScrollableChild not being calculated correctly for vertical carousels
- Return current from the hook
- Return an array of indexes for the children shown within the slider view

## [0.0.4]

- `ref` accepts HTMLUListElement and HTMLDivElement types without casting, while still accepting generic HTMLElement type

## [0.0.3]

- Update documentation in README.md
- Added CHANGELOG.md
- Added TODO.md

## [0.0.2]

- Added option to go to previous/next slide a specific amount
- Updates to package.json
  - Added keywords
  - Added bugs URL
- Fixed issue with going to last item from the start where multiple items are in view
- Implement [@use-effect/did-update](https://www.npmjs.com/package/@use-effect/did-update) to stop unneeded renders on mount

## [0.0.1] (removed)

Initial release
