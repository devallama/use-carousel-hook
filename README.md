# useCarousel

[![License: ISC](https://img.shields.io/badge/License-ISC-blue.svg)](https://gitlab.com/devallama/use-carousel-hook/-/blob/main/LICENSE)
[![npm version](https://img.shields.io/npm/v/use-carousel-hook.svg)](https://www.npmjs.com/package/use-carousel-hook)
[![Test coverage](https://gitlab.com/devallama/use-carousel-hook/badges/main/coverage.svg)](https://gitlab.com/devallama/use-carousel-hook/badges/main/coverage.svg)

useCarousel is a React hook for adding functions to create a sliding carousel.

No styling is included, the hook returns functions to integrate into your slider to allow for full flexibility and control over the carousel you create.

Typescript definitions are included.

## Demo

Take a look at the demo on CodeSandbox which includes styles to demonstate how to use the hook.

[https://codesandbox.io/s/use-carousel-hook-demo-3ledpu?file=/src/Carousel.tsx](https://codesandbox.io/s/use-carousel-hook-demo-3ledpu?file=/src/Carousel.tsx)

## How to use

Call the useCarousel hook in your code. This will return the following values as an object.

| Property   | Type                                     | Description                                                                              |
| ---------- | ---------------------------------------- | ---------------------------------------------------------------------------------------- |
| ref        | React.MutableRefObject\<HTMLElement>     | Attach this ref to your carousel element that contains your cards/elements.              |
| previous   | (amount: number = 1) => void;            | Go to the previous element in the carousel. Can set amount to decrease by, default is 1. |
| next       | (amount: number = 1) => void;            | Go to the next element in the carousel. Can set amount to increase by, default is 1.     |
| setCurrent | (current: number) => void;               | Go to a specific element index in the carousel.                                          |
| reset      | () => void;                              | Go to the beginning of the carousel.                                                     |
| position   | { isAtStart: boolean; isAtEnd: boolean } | Position of the carousel. Can be used to disable next/previous buttons if needed.        |
| current    | number;                                  | The index of the current item.                                                           |
| inView     | number[];                                | The indexes of the items in view.                                                        |

### Code example

```react
import React from 'react';
import { useCarousel } from 'use-carousel-hook';

const Carousel: React.FC = () => {
    const { ref, previous, next, setCurrent, reset } = useCarousel<HTMLUListElement>();

    return (
        <div>
            <button onClick={() => previous()}>Previous</button>
            <button onClick={() => previous(2)}>Go back 2 items</button>
            <button onClick={() => next()}>Next</button>
            <button onClick={() => next(2)}>Go forward 2 items</button>
            <button onClick={() => reset()}>Reset</button>
            <button onClick={() => setCurrent(2)}>Set index to 2</button>
            <ul ref={ref}>
                <li>Item 1</li>
                <li>Item 2</li>
                <li>Item 3</li>
                <li>Item 4</li>
            </ul>
        </div>
    );
};

export default Carousel;
```

## Options

| Option         | Type                       | Default value |
| -------------- | -------------------------- | ------------- |
| scrollBehavior | 'smooth' \| 'auto'         | 'smooth'      |
| direction      | 'horizontal' \| 'vertical' | 'horizontal'  |

### Options example

```typescript
const { ref, previous, next } = useCarousel({ scrollBehavior: 'auto', direction: 'vertical' });
```

### ESNext

A ESNext build of the package can be imported using `use-carousel-hook/esnext`.
