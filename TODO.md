# TODO for version 1

## Priority: high - target v0.1

- Add unit tests ✔️ [added in 0.1.0]
- Run tests on GitLab CI/CD ✔️ [added in 0.0.13]
- Run tests, publish, and build package on CI/CD ✔️ [added in 0.0.11]

## Priority: normal - target v0.2

- Create example carousels ✔️ [added in 0.2.0]
- Export the `current` index of the slide ✔️ [added in 0.0.5]
- Add issue templates for bugs and feature requests

## Priority: low - target v0.3

- Create CSS with basic styling for carousels
- Create styled-components with basic styling for carousels
