import React, { useState, useRef, useEffect, useReducer, useCallback } from 'react';
import useDidUpdateEffect from '@use-effect/did-update';
import reducer from './reducer';

export type Interaction = 'scroll' | 'button' | null;

interface Options<RefElement> {
    scrollBehavior?: ScrollBehavior;
    direction?: 'horizontal' | 'vertical';
    initialRef?: React.MutableRefObject<RefElement>;
}

interface CarouselPosition {
    isAtStart: boolean;
    isAtEnd: boolean;
}

interface CarouselHook<RefElement> {
    ref: React.RefObject<RefElement>;
    previous: (amount?: number, interaction?: Interaction) => void;
    next: (amount?: number, interaction?: Interaction) => void;
    setCurrent: (current: number, interaction?: Interaction) => void;
    reset: () => void;
    position: CarouselPosition;
    current: number;
    inView: number[];
}

const useCarousel = <RefElement extends HTMLElement>(
    options: Options<RefElement> = {}
): CarouselHook<RefElement> => {
    const { direction = 'horizontal' } = options;
    const ref = options.initialRef ? options.initialRef : useRef<RefElement>(null);
    const [{ current, interaction }, dispatch] = useReducer(reducer, {
        current: 0,
        interaction: null
    });
    const [maxIndex, setMaxIndex] = useState(-1);
    // State to track whether the user is currently scrolling
    const [isUserScroll, setIsUserScroll] = useState<boolean | null>(null);
    // Current position of the carousel
    const [position, setPosition] = useState<CarouselPosition>({ isAtStart: true, isAtEnd: false });
    // Elements current in view
    const [inView, setInView] = useState([0]);

    const updateMaxIndex = useCallback(() => {
        if (!ref.current) return;
        const el = ref.current;
        const children = <HTMLElement[]>[...el.children];

        // If scrollWidth or scrollHeight is 0, return the children length
        if ((direction === 'vertical' && el.scrollHeight === 0) || el.scrollWidth === 0) {
            setMaxIndex(children.length - 1);
            return;
        }

        const lastScrollableChild = children.findIndex(child => {
            return direction === 'vertical'
                ? child.offsetTop >= el.scrollHeight - el.offsetHeight
                : child.offsetLeft >= el.scrollWidth - el.offsetWidth;
        });

        setMaxIndex(lastScrollableChild >= 0 ? lastScrollableChild : children.length - 1);
    }, [ref.current]);

    // Update in view state to find which elements are currently in the carousel view
    const updateInView = () => {
        if (!ref.current) return;

        const carouselEl = ref.current;
        const childEls = [...carouselEl.children] as HTMLElement[];
        const currentChildEl = childEls[current];
        const inView = [current];
        const carouselBottom = currentChildEl.offsetTop + carouselEl.offsetHeight;
        const carouselRight = currentChildEl.offsetLeft + carouselEl.offsetWidth;

        // Find which elements are in view
        for (let index = current + 1; index < childEls.length; index++) {
            if (direction === 'vertical') {
                if (childEls[index].offsetTop + childEls[index].offsetHeight <= carouselBottom) {
                    inView.push(index);
                } else {
                    break;
                }
            } else {
                if (childEls[index].offsetLeft + childEls[index].offsetWidth <= carouselRight) {
                    inView.push(index);
                } else {
                    break;
                }
            }
        }

        setInView(inView);
    };

    // Update the current index while the user is scrolling
    const updateCurrentOnScroll = (carouselEl: RefElement, current: number) => {
        for (let index = 0; index < carouselEl.children.length; index++) {
            const child = carouselEl.children[index] as HTMLElement;

            if (
                (direction === 'horizontal' && child.offsetLeft >= carouselEl.scrollLeft) ||
                (direction === 'vertical' && child.offsetTop >= carouselEl.scrollTop)
            ) {
                if (index !== current) {
                    setCurrent(index, 'scroll');
                }
                break;
            }
        }
    };

    // Add events to handle setting of isUserScroll
    useEffect(() => {
        const mouseDownHandler = () => setIsUserScroll(true);
        const mouseUpHandler = () => setIsUserScroll(false);

        ref.current?.addEventListener('mousedown', mouseDownHandler);
        ref.current?.addEventListener('mouseup', mouseUpHandler);

        return () => {
            ref.current?.removeEventListener('mousedown', mouseDownHandler);
            ref.current?.removeEventListener('mouseup', mouseUpHandler);
        };
    }, []);

    // Attach listeners to update the maxIndex and inView state when resizing
    useEffect(() => {
        window.addEventListener('resize', updateMaxIndex);
        window.addEventListener('resize', updateInView);

        return () => {
            window.removeEventListener('resize', updateMaxIndex);
            window.removeEventListener('resize', updateInView);
        };
    }, [current]);

    useEffect(() => {
        // When current updates, attach a new event handler with the updated
        // current value to call updateCurrentOnScroll
        const scrollHandler = () =>
            isUserScroll && ref.current && updateCurrentOnScroll(ref.current, current);

        ref.current?.addEventListener('scroll', scrollHandler);

        return () => {
            ref.current?.removeEventListener('scroll', scrollHandler);
        };
    }, [current, isUserScroll]);

    // Update maxIndex inView state if the ref element changes
    useEffect(() => {
        updateMaxIndex();
        updateInView();
    }, [ref.current]);

    useDidUpdateEffect(() => {
        if (!ref.current) return;
        // If interaction is scroll, the carousel doesn't need to handle the scrolling
        if (interaction !== 'scroll') {
            const carouselEl = ref.current;
            const currentChildEl = carouselEl.children[current] as HTMLElement;

            // Scroll to the element
            if (direction === 'vertical') {
                carouselEl.scrollTo({
                    top: currentChildEl.offsetTop,
                    behavior: options.scrollBehavior || 'smooth'
                });
            } else {
                carouselEl.scrollTo({
                    left: currentChildEl.offsetLeft,
                    behavior: options.scrollBehavior || 'smooth'
                });
            }
        }

        // Update the inView items
        updateInView();
        // Not a user scroll so set to false
        setIsUserScroll(false);

        // Update the carousel position
        setPosition({
            isAtStart: current === 0,
            isAtEnd: current === maxIndex
        });
    }, [current]);

    const previous = (amount = 1, interaction: Interaction = 'button'): void => {
        dispatch({ type: 'decrement', amount, maxIndex, interaction });
    };

    const next = (amount = 1, interaction: Interaction = 'button'): void => {
        dispatch({ type: 'increment', amount, maxIndex, interaction });
    };

    const setCurrent = (current: number, interaction: Interaction = 'button'): void => {
        dispatch({ type: 'set', current, interaction });
    };

    const reset = (): void => {
        dispatch({ type: 'reset', interaction: null });
    };

    return {
        ref,
        previous,
        next,
        setCurrent,
        reset,
        position,
        current,
        inView
    };
};

export default useCarousel;
