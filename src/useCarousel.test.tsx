import { renderHook, act } from '@testing-library/react-hooks';
import useCarousel from './useCarousel';

const childCount = 5;

interface MockCarousel extends HTMLUListElement {
    offsetWidth: number;
    offsetHeight: number;
    scrollWidth: number;
    scrollHeight: number;
    scrollLeft: number;
    scrollTop: number;
}

const mockCarousel: MockCarousel = document.createElement('ul');

for (let i = 0; i <= childCount; i++) {
    const child = document.createElement('li');

    Object.defineProperties(child, {
        offsetLeft: {
            get: () => 200 * i
        },
        offsetTop: {
            get: () => 200 * i
        },
        offsetHeight: {
            get: () => 200
        },
        offsetWidth: {
            get: () => 200
        }
    });

    mockCarousel.appendChild(child);
}

beforeAll(() => {
    Object.defineProperties(mockCarousel, {
        scrollTo: {
            get: () => jest.fn()
        }
    });
    Object.defineProperty(mockCarousel, 'offsetHeight', { writable: true, value: 200 });
    Object.defineProperty(mockCarousel, 'offsetWidth', { writable: true, value: 200 });
    Object.defineProperty(mockCarousel, 'scrollTop', { writable: true, value: 0 });
    Object.defineProperty(mockCarousel, 'scrollLeft', { writable: true, value: 0 });
    Object.defineProperty(mockCarousel, 'scrollWidth', {
        writable: true,
        value: 200 * childCount
    });
    Object.defineProperty(mockCarousel, 'scrollHeight', {
        writable: true,
        value: 200 * childCount
    });
});

beforeEach(() => {
    mockCarousel.offsetHeight = 200;
    mockCarousel.offsetWidth = 200;
    mockCarousel.scrollTop = 0;
    mockCarousel.scrollLeft = 0;
    mockCarousel.scrollWidth = 200 * childCount;
    mockCarousel.scrollHeight = 200 * childCount;
});

test(`works with scrollBehavior 'auto'`, () => {
    const { result } = renderHook(() =>
        useCarousel({ initialRef: { current: mockCarousel }, scrollBehavior: 'auto' })
    );

    expect(result.current.current).toBe(0);

    act(() => {
        result.current.next();
    });

    expect(result.current.current).toBe(1);
});

test(`works without initialRef`, () => {
    const { result } = renderHook(() => useCarousel());

    expect(result.current.current).toBe(0);

    act(() => {
        result.current.setCurrent(1);
    });

    expect(result.current.current).toBe(1);
});

test(`doesn't error if scrollWidth is 0`, () => {
    mockCarousel.scrollWidth = 0;

    const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

    expect(result.error).toBe(undefined);
});

test('unmounts without useEffect errors', () => {
    const { result, unmount } = renderHook(() => useCarousel());

    unmount();

    expect(result.error).toBeUndefined();
});

test(`doesn't error if the scroll width is larger than the total width of elements`, () => {
    mockCarousel.scrollWidth = 400 * childCount;

    const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

    expect(result.error).toBe(undefined);
});

test(`doesn't error when scrolling without a ref set`, () => {
    const { result } = renderHook(() => useCarousel());

    act(() => {
        mockCarousel.dispatchEvent(new Event('mousedown'));
    });

    act(() => {
        mockCarousel.dispatchEvent(new Event('scroll'));
    });

    act(() => {
        mockCarousel.dispatchEvent(new Event('mouseup'));
    });

    expect(result.error).toBe(undefined);
});

test(`doesn't update current if scrolling but scroll position doesn't change`, () => {
    const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

    act(() => {
        mockCarousel.dispatchEvent(new Event('mousedown'));
    });

    act(() => {
        mockCarousel.dispatchEvent(new Event('scroll'));
    });

    act(() => {
        mockCarousel.dispatchEvent(new Event('mouseup'));
    });

    expect(result.current.current).toBe(0);
});

describe('horizontal tests', () => {
    test('calling next() increases current by 1', () => {
        const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

        act(() => {
            result.current.next();
        });

        expect(result.current.current).toBe(1);
    });

    test('calling next(2) increases current by 2', () => {
        const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

        act(() => {
            result.current.next(2);
        });

        expect(result.current.current).toBe(2);
    });

    test('carousel goes back to the start when going to next slide on maxIndex', () => {
        const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

        act(() => {
            result.current.next(childCount + 1);
        });

        expect(result.current.current).toBe(0);
    });

    test('calling previous() decreases curernt by 1', () => {
        const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

        act(() => {
            result.current.next(2);
        });

        expect(result.current.current).toBe(2);

        act(() => {
            result.current.previous();
        });

        expect(result.current.current).toBe(1);
    });

    test('calling previous(2) decreases current by 2', () => {
        const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

        act(() => {
            result.current.next(4);
        });

        expect(result.current.current).toBe(4);

        act(() => {
            result.current.previous(2);
        });

        expect(result.current.current).toBe(2);
    });

    test('goes to the end when calling previous() from 0 index', () => {
        const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

        act(() => {
            result.current.previous();
        });

        expect(result.current.current).toBe(childCount - 1);
    });

    test('setting current with setCurrent', () => {
        const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

        act(() => {
            result.current.setCurrent(3);
        });

        expect(result.current.current).toBe(3);
    });

    test('resets current to 0 when not 0 with reset()', () => {
        const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

        act(() => {
            result.current.setCurrent(2);
        });

        expect(result.current.current).toBe(2);

        act(() => {
            result.current.reset();
        });

        expect(result.current.current).toBe(0);
    });

    test('shows multiple items inView', () => {
        mockCarousel.offsetWidth = 400;

        const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

        expect(result.current.inView).toMatchObject([0, 1]);

        act(() => {
            result.current.setCurrent(2);
        });

        expect(result.current.inView).toMatchObject([2, 3]);
    });

    test('updates current on scroll', () => {
        mockCarousel.scrollLeft = 400;

        const { result } = renderHook(() => useCarousel({ initialRef: { current: mockCarousel } }));

        act(() => {
            mockCarousel.dispatchEvent(new Event('mousedown'));
        });

        act(() => {
            mockCarousel.dispatchEvent(new Event('scroll'));
        });

        act(() => {
            mockCarousel.dispatchEvent(new Event('mouseup'));
        });

        expect(result.current.current).toBe(2);
    });
});

describe('vertical tests', () => {
    test('calling next() increases current by 1', () => {
        const { result } = renderHook(() =>
            useCarousel({ initialRef: { current: mockCarousel }, direction: 'vertical' })
        );

        act(() => {
            result.current.next();
        });

        expect(result.current.current).toBe(1);
    });

    test('calling next(2) increases current by 2', () => {
        const { result } = renderHook(() =>
            useCarousel({ initialRef: { current: mockCarousel }, direction: 'vertical' })
        );

        act(() => {
            result.current.next(2);
        });

        expect(result.current.current).toBe(2);
    });

    test('carousel goes back to the start when going to next slide on maxIndex', () => {
        const { result } = renderHook(() =>
            useCarousel({ initialRef: { current: mockCarousel }, direction: 'vertical' })
        );

        act(() => {
            result.current.next(childCount + 1);
        });

        expect(result.current.current).toBe(0);
    });

    test('calling previous() decreases curernt by 1', () => {
        const { result } = renderHook(() =>
            useCarousel({ initialRef: { current: mockCarousel }, direction: 'vertical' })
        );

        act(() => {
            result.current.next(2);
        });

        expect(result.current.current).toBe(2);

        act(() => {
            result.current.previous();
        });

        expect(result.current.current).toBe(1);
    });

    test('calling previous(2) decreases current by 2', () => {
        const { result } = renderHook(() =>
            useCarousel({ initialRef: { current: mockCarousel }, direction: 'vertical' })
        );

        act(() => {
            result.current.next(4);
        });

        expect(result.current.current).toBe(4);

        act(() => {
            result.current.previous(2);
        });

        expect(result.current.current).toBe(2);
    });

    test('goes to the end when calling previous() from 0 index', () => {
        const { result } = renderHook(() =>
            useCarousel({ initialRef: { current: mockCarousel }, direction: 'vertical' })
        );

        act(() => {
            result.current.previous();
        });

        expect(result.current.current).toBe(childCount - 1);
    });

    test('setting current with setCurrent', () => {
        const { result } = renderHook(() =>
            useCarousel({ initialRef: { current: mockCarousel }, direction: 'vertical' })
        );

        act(() => {
            result.current.setCurrent(3);
        });

        expect(result.current.current).toBe(3);
    });

    test('resets current to 0 when not 0 with reset()', () => {
        const { result } = renderHook(() =>
            useCarousel({ initialRef: { current: mockCarousel }, direction: 'vertical' })
        );

        act(() => {
            result.current.setCurrent(2);
        });

        expect(result.current.current).toBe(2);

        act(() => {
            result.current.reset();
        });

        expect(result.current.current).toBe(0);
    });

    test('shows multiple items inView', () => {
        mockCarousel.offsetHeight = 400;

        const { result } = renderHook(() =>
            useCarousel({ initialRef: { current: mockCarousel }, direction: 'vertical' })
        );

        expect(result.current.inView).toMatchObject([0, 1]);

        act(() => {
            result.current.setCurrent(2);
        });

        expect(result.current.inView).toMatchObject([2, 3]);
    });

    test('updates current on scroll', () => {
        mockCarousel.scrollTop = 400;

        const { result } = renderHook(() =>
            useCarousel({ initialRef: { current: mockCarousel }, direction: 'vertical' })
        );

        act(() => {
            mockCarousel.dispatchEvent(new Event('mousedown'));
        });

        act(() => {
            mockCarousel.dispatchEvent(new Event('scroll'));
        });

        act(() => {
            mockCarousel.dispatchEvent(new Event('mouseup'));
        });

        expect(result.current.current).toBe(2);
    });
});
