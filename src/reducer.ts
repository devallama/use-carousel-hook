import type { Interaction } from './useCarousel';

type ActionOption = { interaction: Interaction };

type Action =
    | (ActionOption & { type: 'increment'; maxIndex: number; amount: number })
    | (ActionOption & { type: 'decrement'; maxIndex: number; amount: number })
    | (ActionOption & { type: 'reset' })
    | (ActionOption & { type: 'set'; current: number });

interface State {
    current: number;
    interaction: Interaction;
}

const reducer = (state: State, action: Action): State => {
    switch (action.type) {
        case 'increment': {
            const current = state.current + action.amount;
            return {
                current: current > action.maxIndex ? 0 : current,
                interaction: action.interaction
            };
        }
        case 'decrement': {
            const current = state.current - action.amount;
            return {
                current: current < 0 ? action.maxIndex : current,
                interaction: action.interaction
            };
        }
        case 'set':
            return {
                current: action.current,
                interaction: action.interaction
            };
        case 'reset':
            return { current: 0, interaction: action.interaction };
    }
};

export default reducer;
